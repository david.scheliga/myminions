# myminions
[![Coverage Status](https://coveralls.io/repos/gitlab/david.scheliga/myminions/badge.svg?branch=release)](https://coveralls.io/gitlab/david.scheliga/myminions?branch=release)
[![Build Status](https://travis-ci.com/david.scheliga/myminions.svg?branch=release)](https://travis-ci.com/david.scheliga/myminions)
[![PyPi](https://img.shields.io/pypi/v/myminions.svg?style=flat-square&label=PyPI)](https://https://pypi.org/project/myminions/)
[![Python Versions](https://img.shields.io/pypi/pyversions/myminions.svg?style=flat-square&label=PyPI)](https://https://pypi.org/project/myminions/)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Documentation Status](https://readthedocs.org/projects/myminions/badge/?version=latest)](https://myminions.readthedocs.io/en/latest/?badge=latest)


*My minions* is a loose collection of frequently used methods doing basic 
bidding's. [Read-the-docs](https://myminions.readthedocs.io/en/latest/)

![myminions icon](https://myminions.readthedocs.io/en/latest/_images/myminions_icon.png "3 minions")

## Installing

Installing the latest release using pip is recommended.

````shell script
$ pip install myminions
````

## Authors

* **David Scheliga** 
    [@gitlab](https://gitlab.com/david.scheliga)
    [@Linkedin](https://www.linkedin.com/in/david-scheliga-576984171/)
    - Initial work
    - Maintainer

## License

This project is licensed under the GNU GENERAL PUBLIC LICENSE - see the
[LICENSE](https://gitlab.com/david.scheliga/dicthandling/blob/master/LICENSE) file for details

## Acknowledge

- [Code style: black](https://github.com/psf/black)
- [PurpleBooth](https://gist.github.com/PurpleBooth/109311bb0361f32d87a2)
- [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
